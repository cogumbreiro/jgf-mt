compile:
	(cd jgfutil; make)
	(cd section1; make)
	(cd section2; make)
	(cd section3; make)
clean:
	(cd section1; make clean)
	(cd section2; make clean)
	(cd section3; make clean)
	(cd jgfutil; make clean)

