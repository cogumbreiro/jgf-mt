
/**************************************************************************
*                                                                         *
*             Java Grande Forum Benchmark Suite - Version 2.0             *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         * 
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/


import jgfutil.*; 

public class BarrierBench {

  public static int nthreads;

  private static void usage() {
    System.err.println("Usage: BarrierBench NTHREADS A");
    System.err.println("\t\tNTHREADS the number of threads 1..");
    System.exit(255);
  }

  public static void main (String argv[]){

  if(argv.length == 2 ) {
    nthreads = Integer.parseInt(argv[0]);
    String input_size = argv[1].toUpperCase();
    if (! input_size.equals("A")) {
        usage();
        return;
    }
  } else {
    usage();
    return;
  }
      if(argv.length != 0 ) {
        nthreads = Integer.parseInt(argv[0]);
      } else {
        System.out.println("The no of threads has not been specified, defaulting to 1");
        System.out.println("  ");
        nthreads = 1;
      }

    JGFInstrumentor.printHeader(1,0,nthreads);

    JGFBarrierBench tb = new JGFBarrierBench(nthreads);
    tb.JGFrun();
  }

}
