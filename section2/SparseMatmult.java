/**************************************************************************
*                                                                         *
*         Java Grande Forum Benchmark Suite - Thread Version 1.0          *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         * 
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/


import sparsematmult.*; 
import jgfutil.*; 

public class SparseMatmult{ 

  public static int nthreads;

  private static void usage() {
    System.err.println("Usage: SparseMatmult NTHREADS INPUT_SIZE");
    System.err.println("\t\tNTHREADS the number of threads 1..");
    System.err.println("\t\tINPUT_SIZE either A, B, or C");
    System.exit(255);
  }

  public static void main(String argv[]){
    int size;
  if(argv.length == 2 ) {
    nthreads = Integer.parseInt(argv[0]);
    String input_size = argv[1].toUpperCase();
    if (input_size.equals("A")) {
        size = 0;
    } else if (input_size.equals("B")) {
        size = 1;
    } else if (input_size.equals("C")) {
        size = 2;
    } else {
        usage();
        return;
    }
  } else {
    usage();
    return;
  }


    JGFInstrumentor.printHeader(2,0,nthreads);

    JGFSparseMatmultBench smm = new JGFSparseMatmultBench(nthreads); 
    smm.JGFrun(0);
 
  }
}
