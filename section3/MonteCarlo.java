/**************************************************************************
*                                                                         *
*         Java Grande Forum Benchmark Suite - Thread Version 1.0          *
*                                                                         *
*                            produced by                                  *
*                                                                         *
*                  Java Grande Benchmarking Project                       *
*                                                                         *
*                                at                                       *
*                                                                         *
*                Edinburgh Parallel Computing Centre                      *
*                                                                         * 
*                email: epcc-javagrande@epcc.ed.ac.uk                     *
*                                                                         *
*                                                                         *
*      This version copyright (c) The University of Edinburgh, 2001.      *
*                         All rights reserved.                            *
*                                                                         *
**************************************************************************/


import montecarlo.*;

import jgfutil.*;

public class MonteCarlo { 

  public static int nthreads;

  private static void usage() {
    System.err.println("Usage: MonteCarlo NTHREADS INPUT_SIZE");
    System.err.println("\t\tNTHREADS the number of threads 1..");
    System.err.println("\t\tINPUT_SIZE either A or B");
    System.exit(255);
  }
  
  public static void main(String argv[]){
   
    int size; 

  if(argv.length == 2 ) {
    nthreads = Integer.parseInt(argv[0]);
    String input_size = argv[1].toUpperCase();
    if (input_size.equals("A")) {
        size = 0;
    } else if (input_size.equals("B")) {
        size = 1;
    } else {
        usage();
        return;
    }
  } else {
    usage();
    return;
  }

    JGFInstrumentor.printHeader(3,size,nthreads);

    JGFMonteCarloBench mc = new JGFMonteCarloBench(nthreads); 
    mc.JGFrun(size);
 
  }
}


